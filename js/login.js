/*
nom du fichier : login.js
date : 25/11/2020
auteur : Mathieu LINDER
*/



var monLogin = document.getElementById("username");
var monMDP = document.getElementById("password");
var monBoutonDeConnexion = document.getElementById("submit");

// fonction connexion au compte
function controleLogin() {
    var leLogin = monLogin.value;
    var leMDP = monMDP.value;
    

    if (leLogin != "") {
        if (leLogin == "Guest") {
            alert("Bonjour Invité");
            // on ouvre la page suivante avec Guest comme connecté
        } else {
            if ((leLogin == "Admin") && (leMDP == "secret")) {
                alert("Bonjour Maitre");
                // on ouvre la page suivante avec Admin comme connecté
            } else {
                if (leLogin == leMDP) {
                    alert("Bonjour " + leLogin);
                    // on ouvre la page suivante avec leLogin comme connecté
                } else {
                    alert("Erreur dans l'association Login/mdp");
                    document.getElementById("identification").action = document.location.href="index.html";
                }
            }
        }
    } else { 
        alert("Veuillez saisir votre identifiant !");
        document.getElementById("identification").action = document.location.href="index.html";
}
}
//  pour controler la connexion d'un utilisateur 
monBoutonDeConnexion.addEventListener("click", controleLogin, false);

