/*
nom du fichier : login.js
date : 23/11/2020
auteur : Mathieu LINDER
*/

// Accordion du filtre ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var acc = document.getElementsByClassName("accordion");
for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        }
  });
}

// Affichage de toutes les BD --------------------------------------------------------------------------------------------------------------------------------------------------------------------
function affichageBD(){
	albums.forEach((album, key) => {
	    serie = series.get(album.idSerie);
      auteur = auteurs.get(album.idAuteur);

// Dommage, c'était plus propre mais ça marche pas comme je veux :( 
      // var divAlbum= document.createElement("div");
      // divAlbum.className = "unAlbum";
      // divAlbum.appendChild(document.createTextNode("Série : " + serie.nom));
      // divAlbum.appendChild(document.createTextNode("Album n° : " + album.numero));
      // divAlbum.appendChild(document.createTextNode("Titre : " + album.titre));
      // divAlbum.appendChild(document.createTextNode("Auteur : " + auteur.nom));
      // divAlbum.appendChild(document.createTextNode("Prix : " + album.prix + " €"));
      // document.getElementById("mesAlbums").appendChild(divAlbum);

      var affichageDesAlbums = document.getElementById("mesAlbums");
      var monImg = serie.nom + "-" + album.numero + "-" + album.titre;
      var monImg = monImg.replace(/'|!|\?|\.|"|:|\$/g, "");
      var rajoutHtml = "<div class=unAlbum>" + "<a>" + "Série : " +  serie.nom + "<br/>" + "Album n° : " + album.numero + "<br/>" + "Titre : "+ album.titre + "<br/>" + "Auteur : " + auteur.nom + "<br/>" + "Prix : " + album.prix  + " €" + "</a>" + '<img class="miniImg" src="albumsMini/' + monImg + ".jpg" + '"' + ">" + '<input type="button" id="ajoutPanier" name=' + key + ' value="Ajoutez au panier">' + "</div>";
      affichageDesAlbums.insertAdjacentHTML("beforeend", rajoutHtml);

      // var b = document.getElementById("ajoutPanier");
      // b.setAttribute("name", album.titre)

      // var divAlbum= document.createElement("img");
      // divAlbum.src= "albumsMini/" + monImg;
      // document.getElementById("mesAlbums").appendChild(divAlbum);

  });
}

document.addEventListener("DOMContentLoaded", affichageBD());




// Remplissage des combobox  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

var comboSerie = document.comboBox.comboSerie;
var comboAuteur = document.comboBox.comboAuteur;

// fonction appelé pour remplir les combobox
function remplissageComboSerie(value) {
    comboSerie.length++;
    comboSerie.options[comboSerie.length - 1].text = value.nom;
}

function remplissageComboAuteur(value) {
comboAuteur.length++;
comboAuteur.options[comboAuteur.length - 1].text = value.nom;
}

// Appel des fonctions de remplissage des combobox
series.forEach(remplissageComboSerie);
auteurs.forEach(remplissageComboAuteur);



// Affichage des BD en fonction du choix des filtres ------------------------------------------------------------------------------------------------------------------------------------------------

var ListeSerie = document.getElementById("listeSerie");
var monAffichage = document.getElementById("Affichage");

ListeSerie.addEventListener("change", function () {
  monAffichage.innerHTML = "Série : ";                // pour que les différents choix n'apparaissent pas à la suite des autres
    getSerie(this)                                          // pour récupérer la Key de la serie selectionnee dans la combobox
});


// fonction a tout faire qui va récupérer la key de la série selectionnee en combobox et appelé les fonctions RechercheIdSerieSelectionnee et affichageBDcombo
function getSerie(element) {
  monAffichage.innerHTML += element.value;
  serieSelectionne = element.value                                      // = le choix dans la comboBox (ex: element.value = Spirou et fantasio)
  series.forEach(RechercheIdSerieSelectionnee);                         // pour que le nom de la serie selectionnee = key correspondante de sa map series.js

  var vidageMesAlbums = "";
  document.getElementById("mesAlbums").innerHTML = vidageMesAlbums;     // pour supprimer l'affichage de tous les albums non filtrées

  albums.forEach(affichageBDcombo);                                     // on ne met dans Combo Ville que les villes du pays
}

// appelé par getSerie - correspondance entre serieSelectionne et sa key dans series.js
function RechercheIdSerieSelectionnee(value, key) {  
  if (value.nom == serieSelectionne) { 
      idSerieCombo = key;                                               // la serie selectionnée dans la comboBox prend enfin la valeur de sa Key correspondante... Enfin... (ex: Spirou et fantasio = 2)
  }
}

// appelé par getSerie pour afficher les albums correspondant au choix de la combo
function affichageBDcombo(value, key) {
  var numeroSerie = value.idSerie;
  if (numeroSerie == idSerieCombo) {
	    serie = series.get(value.idSerie);
      auteur = auteurs.get(value.idAuteur);

      var affichageDesAlbums = document.getElementById("mesAlbums");
      var monImg = serie.nom + "-" + value.numero + "-" + value.titre;
      var monImg = monImg.replace(/'|!|\?|\.|"|:|\$/g, "");
      var rajoutHtml = "<div class=unAlbum>" + "<a>" + "Série : " +  serie.nom + "<br/>" + "Album n° : " + value.numero + "<br/>" + "Titre : "+ value.titre + "<br/>" + "Auteur : " + auteur.nom + "<br/>" + "Prix : " + value.prix  + " €" + "</a>" + '<img class="miniImg" src="albumsMini/' + monImg + ".jpg" + '"' + ">" + '<input type="button" id="ajoutPanier" name=' + key + ' value="Ajoutez au panier">' + "</div>";
      affichageDesAlbums.insertAdjacentHTML("beforeend", rajoutHtml);
  }
}


